package by.freelance.interviews.repository;

import by.freelance.interviews.utils.DatabaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;

    @BeforeEach
    public void setUp() {

        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }
}
