package by.freelance.interviews.utils;

import by.freelance.interviews.repository.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class DatabaseCleaner {

    @Autowired
    private UserRepository userRepository;

    public void clean() {
        userRepository.deleteAll();
    }
}
