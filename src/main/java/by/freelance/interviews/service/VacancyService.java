package by.freelance.interviews.service;

import by.freelance.interviews.dto.*;
import by.freelance.interviews.entity.InterviewEntity;
import by.freelance.interviews.entity.UserEntity;
import by.freelance.interviews.entity.VacancyEntity;
import by.freelance.interviews.mapper.VacancyMapper;
import by.freelance.interviews.repository.InterviewRepository;
import by.freelance.interviews.repository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VacancyService {

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private VacancyMapper vacancyMapper;

    public VacancyFullDto findById(int id) {
        VacancyEntity vacancy = vacancyRepository.findById(id);
        if (vacancy == null) {
            throw new RuntimeException("User not found by id: " + id);
        }
        System.out.println("VacancyService -> Found vacancy: " + vacancy);
        VacancyFullDto dto = vacancyMapper.map(vacancy);
        return dto;
    }

    public List<VacancyShortDto> findAll() {
        List<VacancyEntity> allVacancy = vacancyRepository.findAll();
        List<VacancyShortDto> allDtos = vacancyMapper.map(allVacancy);
        return allDtos;
    }

    public VacancyFullDto update(VacancyUpdateDto updateRequest) {

        if (updateRequest.getId() == null ) {
            throw new RuntimeException("VacancyService -> Can't update entity without id");
        }
        if (vacancyRepository.findById(updateRequest.getId()) == null) {
            throw new RuntimeException("VacancyService -> User not found by id: " + updateRequest.getId());
        }

        VacancyEntity vacancy = vacancyRepository.findById(updateRequest.getId());
        vacancy.setName(updateRequest.getName());
        vacancy.setPosition(updateRequest.getPosition());
        vacancy.setSalary(updateRequest.getSalary());
        vacancy.setCompanyName(updateRequest.getCompanyName());

        VacancyEntity updateVacancy = vacancyRepository.update(vacancy);
        System.out.println("VacancyService -> Updated vacancy: " + vacancy);
        VacancyFullDto updatedDto = vacancyMapper.map(updateVacancy);
        return updatedDto;
    }

    public VacancyFullDto create(VacancyCreateDto createRequest) {
        VacancyEntity vacancy = vacancyMapper.map(createRequest);
        if (vacancy.getId() != null) {
            throw new RuntimeException("VacancyService -> Can't create entity which already has id");
        }
        List<VacancyEntity> existingVacancy = vacancyRepository.findAll();
        for (VacancyEntity existVacancy : existingVacancy) {
            throw new RuntimeException("Email: " + vacancy.getName() + "is taken");
        }
        VacancyEntity createdVacancy = vacancyRepository.create(vacancy);
        System.out.println("VacancyService -> Created vacancy: " + vacancy);
        VacancyFullDto createdDto = vacancyMapper.map(createdVacancy);
        return createdDto;
    }

    public void deleteById(int id) {
        VacancyEntity vacancyToDelete = vacancyRepository.findById(id);
        if (vacancyToDelete == null) {
            throw  new RuntimeException("Vacancy was not found by id: " + id);
        }

        List<InterviewEntity> existingInterviews = vacancyToDelete.getInterviews();

        for (InterviewEntity interview : existingInterviews) {
            interviewRepository.deleteById(interview.getId());
        }

        vacancyRepository.deleteById(id);
    }

}
