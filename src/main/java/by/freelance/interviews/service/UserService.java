package by.freelance.interviews.service;

import by.freelance.interviews.dto.UserCreateDto;
import by.freelance.interviews.dto.UserFullDto;
import by.freelance.interviews.dto.UserShortDto;
import by.freelance.interviews.dto.UserUpdateDto;
import by.freelance.interviews.entity.InterviewEntity;
import by.freelance.interviews.entity.UserEntity;
import by.freelance.interviews.mapper.UserMapper;
import by.freelance.interviews.repository.InterviewRepository;
import by.freelance.interviews.repository.UserRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private UserMapper userMapper;

    public UserFullDto findById(int id) {
        UserEntity user = userRepository.findById(id);
        if (user == null) {
            throw new RuntimeException("User not found by id: " + id);
        }
        System.out.println("UserService -> Found user: " + user);

        UserFullDto dto = userMapper.map(user);
        return dto;
    }

    public List<UserShortDto> findAll() {
        List<UserEntity> allUsers = userRepository.findAll();
        List<UserShortDto> allDtos = userMapper.map(allUsers);
        return allDtos;
    }

    public UserFullDto update(UserUpdateDto updateRequest) {

        if (updateRequest.getId() == null ) {
            throw new RuntimeException("UserService -> Can't update entity without id");
        }
        if (userRepository.findById(updateRequest.getId()) == null) {
            throw new RuntimeException("UserService -> User not found by id: " + updateRequest.getId());
        }

        UserEntity user = userRepository.findById(updateRequest.getId());
        user.setRole(updateRequest.getRole());
        user.setFirstName(updateRequest.getFirstName());
        user.setLastName(updateRequest.getLastName());
        user.setPhone(updateRequest.getPhone());
        user.setYearsOfExperience(updateRequest.getYearsOfExperience());

        UserEntity updateUser = userRepository.update(user);
        System.out.println("UserService -> Updated user: " + user);
        UserFullDto updatedDto = userMapper.map(updateUser);
        return updatedDto;
    }


    public UserFullDto create(UserCreateDto createRequest) {
        UserEntity user = userMapper.map(createRequest);
        if (user.getId() != null) {
            throw new RuntimeException("UserService -> Can't create entity which already has id");
        }
        List<UserEntity> existingUsers = userRepository.findAll();
        for (UserEntity existingUser : existingUsers) {
            throw new RuntimeException("Email: " + user.getEmail() + "is taken");
        }
        UserEntity createdUser = userRepository.create(user);
        System.out.println("UserService -> Created user: " + user);
        UserFullDto createdDto = userMapper.map(createdUser);
        return createdDto;
    }

    public void deleteById(int id) {
        UserEntity userToDelete = userRepository.findById(id);
        if (userToDelete == null) {
            throw  new RuntimeException("User was not found by id: " + id);
        }

        List<InterviewEntity> existingInterviews = userToDelete.getInterviews();

        for (InterviewEntity interview : existingInterviews) {
            interviewRepository.deleteById(interview.getId());
        }

        userRepository.deleteById(id);
    }
}
