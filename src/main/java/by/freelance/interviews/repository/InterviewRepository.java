package by.freelance.interviews.repository;

import by.freelance.interviews.entity.InterviewEntity;
import by.freelance.interviews.entity.UserEntity;

import java.util.List;

public interface InterviewRepository {

    InterviewEntity findById(int id);

    List<InterviewEntity> findAll();

    InterviewEntity create(InterviewEntity entity);

    InterviewEntity update(InterviewEntity entity);

    void deleteById(int id);

    void deleteAll();

}
