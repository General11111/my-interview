package by.freelance.interviews.repository;

import by.freelance.interviews.entity.UserEntity;
import by.freelance.interviews.entity.VacancyEntity;

import java.util.List;

public interface VacancyRepository {

    VacancyEntity findById(int id);

    List<VacancyEntity> findAll();

    VacancyEntity create(VacancyEntity entity);

    VacancyEntity update(VacancyEntity entity);

    void deleteById(int id);

    void deleteAll();

}
