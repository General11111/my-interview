package by.freelance.interviews.mapper;

import by.freelance.interviews.dto.InterviewShortDto;
import by.freelance.interviews.entity.InterviewEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InterviewMapper {

//    @Autowired
//    private UserMapper userMapper;

    public List<InterviewShortDto> map(List<InterviewEntity> entities) {
        List<InterviewShortDto> dtos = new ArrayList<>();
        for (InterviewEntity entity : entities) {
            InterviewShortDto dto = new InterviewShortDto();
            dto.setId(entity.getId());
            dto.setStatus(entity.getStatus());
            dto.setDate(entity.getDate());
            dtos.add(dto);
        }
        return dtos;
    }

}
