package by.freelance.interviews.mapper;

import by.freelance.interviews.dto.InterviewShortDto;
import by.freelance.interviews.dto.UserCreateDto;
import by.freelance.interviews.dto.UserFullDto;
import by.freelance.interviews.dto.UserShortDto;
import by.freelance.interviews.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    @Autowired
    InterviewMapper interviewMapper;

    public UserFullDto map(UserEntity entity) {
        UserFullDto dto = new UserFullDto();
        dto.setId(entity.getId());
        dto.setRole(entity.getRole());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPhone(entity.getPhone());
        dto.setEmail(entity.getEmail());
        dto.setYearsOfExperience(entity.getYearsOfExperience());

        List<InterviewShortDto> interviewDtos = interviewMapper.map(entity.getInterviews());
        dto.setInterviews(interviewDtos);

        return dto;
    }

    public List<UserShortDto> map(List<UserEntity> entities) {
        List<UserShortDto> dtos = new ArrayList<>();
        for (UserEntity entity : entities) {
            UserShortDto dto = new UserShortDto();
            dto.setId(entity.getId());
            dto.setRole(entity.getRole());
            dto.setFirstName(entity.getFirstName());
            dto.setLastName(entity.getLastName());
            dto.setPhone(entity.getPhone());
            dto.setEmail(entity.getEmail());
            dto.setYearsOfExperience(entity.getYearsOfExperience());
            dtos.add(dto);
        }
        return dtos;
    }

    public UserEntity map(UserCreateDto dto) {
        UserEntity entity = new UserEntity();
        entity.setRole(dto.getRole());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setPhone(dto.getPhone());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setYearsOfExperience(dto.getYearsOfExperience());
        return entity;
    }


}
