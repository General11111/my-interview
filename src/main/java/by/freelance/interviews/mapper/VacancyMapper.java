package by.freelance.interviews.mapper;

import by.freelance.interviews.dto.*;
import by.freelance.interviews.entity.VacancyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VacancyMapper {

    @Autowired
    InterviewMapper interviewMapper;

    public VacancyFullDto map(VacancyEntity entity) {
        VacancyFullDto dto = new VacancyFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setPosition(entity.getPosition());
        dto.setSalary(entity.getSalary());
        dto.setCompanyName(entity.getCompanyName());

        List<InterviewShortDto> interviewDtos = interviewMapper.map(entity.getInterviews());
        dto.setInterviews(interviewDtos);

        return dto;
    }

    public List<VacancyShortDto> map(List<VacancyEntity> entities) {
        List<VacancyShortDto> dtos = new ArrayList<>();
        for (VacancyEntity entity : entities) {
            VacancyShortDto dto = new VacancyShortDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setPosition(entity.getPosition());
            dto.setSalary(entity.getSalary());
            dto.setCompanyName(entity.getCompanyName());
            dtos.add(dto);
        }
        return dtos;
    }

    public VacancyEntity map(VacancyCreateDto dto) {
        VacancyEntity entity = new VacancyEntity();
        entity.setName(dto.getName());
        entity.setPosition(dto.getPosition());
        entity.setSalary(dto.getSalary());
        entity.setCompanyName(dto.getCompanyName());
        return entity;
    }


}
