package by.freelance.interviews.dto;

import lombok.Data;

@Data
public class VacancyCreateDto {


    private String name;
    private String position;
    private Integer salary;
    private String companyName;

}
