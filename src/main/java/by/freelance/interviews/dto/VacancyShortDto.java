package by.freelance.interviews.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class VacancyShortDto {

    private Integer id;
    private String name;
    private String position;
    private Integer salary;
    private String companyName;

}
