package by.freelance.interviews.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {

    private Integer id;
    private String role;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private Integer yearsOfExperience;

    private List<InterviewShortDto> interviews = new ArrayList<>();
}
