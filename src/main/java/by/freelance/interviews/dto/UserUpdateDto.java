package by.freelance.interviews.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank(message = "Role can not be blank!")
    @Size(min = 3, max = 20, message = "Role length must be between 3 and 20")
    private String role;

    @NotBlank(message = "FirstName can not be blank!")
    @Size(min = 3, max = 20, message = "FirstName length must be between 3 and 20")
    private String firstName;

    @NotBlank(message = "LastName can not be blank!")
    @Size(min = 3, max = 20, message = "LastName length must be between 3 and 20")
    private String lastName;

    @NotBlank(message = "Phone can not be blank!")
    @Size(min = 3, max = 20, message = "Phone length must be between 3 and 20")
    private String phone;

    @NotBlank(message = "YearsOfExperience can not be blank!")
    @Size(min = 3, max = 20, message = "YearsOfExperience length must be between 3 and 20")
    private Integer yearsOfExperience;


}
