package by.freelance.interviews.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class UserShortDto {

    private Integer id;
    private String role;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private Integer yearsOfExperience;


}
