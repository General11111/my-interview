package by.freelance.interviews.dto;

import lombok.Data;

import java.sql.Date;
import java.util.ArrayList;

@Data
public class InterviewFullDto {

    private Integer id;
    private String status;
    private Date date;
    private UserShortDto user;
    private VacancyShortDto vacancy;


}
