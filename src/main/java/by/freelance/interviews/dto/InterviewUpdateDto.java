package by.freelance.interviews.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class InterviewUpdateDto {

    private Integer id;
    private String status;
    private Date date;
    

}
