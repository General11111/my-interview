package by.freelance.interviews.dto;

import lombok.Data;

import javax.validation.constraints.*;
import java.util.ArrayList;

@Data
public class UserCreateDto {


    @NotBlank(message = "Role can not be blank!")
    @Size(min = 3, max = 20, message = "Role length must be between 3 and 20")
    private String role;

    @NotBlank(message = "FirstName can not be blank!")
    @Size(min = 3, max = 20, message = "FirstName length must be between 3 and 20")
    private String firstName;

    @NotBlank(message = "LastName can not be blank!")
    @Size(min = 3, max = 20, message = "LastName length must be between 3 and 20")
    private String lastName;

    @NotBlank(message = "Phone can not be blank!")
    @Size(min = 3, max = 20, message = "Phone length must be between 3 and 20")
    private String phone;

    @Email(message = "Email must have valid format")
    @NotNull(message = "Email can not be null")
    private String email;

    @NotBlank(message = "Password is not be blank!")
    @Size(min = 8, max = 100, message = "Password length must be between 8 and 100")
    private String password;

    @NotBlank(message = "YearsOfExperience can not be blank!")
    @Size(min = 3, max = 20, message = "YearsOfExperience length must be between 3 and 20")
    private Integer yearsOfExperience;


}
