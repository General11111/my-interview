package by.freelance.interviews.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class InterviewCreateDto {

    private String status;
    private Date date;
    // в мэппере использовать дату и статус
    // а юзер id и ваканси id использовать в сервисе
    private Integer userId;
    private Integer vacancyId;

}
